<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Activity</title>
</head>
<body>
    
    <h1>Divisible by 5</h1>

    <?php divisibleBy(5); ?>

    <h1>Array Manipulation</h1>

    <?php array_push($students, "John Smith") ?>
    <p><?php echo var_dump($students) ?></p>
    <p><?php echo sizeof($students) ?></p>
    <?php array_push($students, "Jane Smith") ?>
    <p><?php echo var_dump($students) ?></p>
    <p><?php echo sizeof($students) ?></p>


    <?php array_shift($students) ?>

    <p><?php echo var_dump($students) ?></p>
    <p><?php echo sizeof($students) ?></p>
</body>
</html>